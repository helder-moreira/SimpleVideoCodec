#include "CAV_Video.h"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

namespace Cav {

void RGB8video::setFile(char* file, bool write){
    if(write){
        m_f = new fstream(file, fstream::out | fstream::binary);
    }
    else{
        ifstream ate_f(file,ifstream::ate|ifstream::binary);
        m_file_size=ate_f.tellg();
        ate_f.close();
        m_f = new fstream(file, fstream::in | fstream::binary);
    }
}

bool RGB8video::is_open() const {
    return m_f->is_open();
}

bool RGB8video::write_header(int lines, int columns){
    m_lines = lines;
    m_columns = columns;
    ostringstream oss;
    oss << m_lines << " " << m_columns << endl;
    m_f->write(oss.str().c_str(), oss.str().size());
    return true;
}

bool RGB8video::writeFrame(Mat image){
    unsigned char* Mi;
    for(int i=0;i<m_lines;i++) {
        Mi = image.ptr<unsigned char>(i);
        for(int j=0;j<m_columns*3;j++) {
            if(!m_f->put(Mi[j])){
                return false;
            }
        }
    }
    return true;
}

bool RGB8video::read_header() {
    string line;

    if (!getline(*m_f,line)) {
        return false;
    }

    // file size - position wo 1st line
    m_data_size = m_file_size - m_f->tellg();

    istringstream tokenizer(line);
    string token;

    getline(tokenizer, token, ' ');
    istringstream i_token(token);
    i_token >> m_lines;

    getline(tokenizer, token, ' ');
    istringstream j_token(token);
    j_token >> m_columns;

    if (m_data_size%(m_columns*m_lines) != 0) {
        cerr << "warning: DATA_SIZE%(COLUMNS*LINES) != 0" << endl;
    }

    return true;
}

int RGB8video::frames() const {
    if (m_data_size%(m_columns*m_lines) != 0) {
        cerr << "warning: DATA_SIZE%(COLUMNS*LINES) != 0" << endl;
    }
    return m_data_size/(m_columns*m_lines*3);
}

bool RGB8video::hasNextFrame() const {
    return !m_f->eof();
}

Mat RGB8video::nextFrame() {
    Mat image(Size(m_columns,m_lines),CV_8UC3);
    char color_comp_value;
    unsigned char* Mi;
    for(int i=0;i<m_lines;i++) {
        Mi = image.ptr<unsigned char>(i);
        for(int j=0;j<m_columns*3;j++) {
            if (m_f->get(color_comp_value)) {
                Mi[j]=color_comp_value;
            } else {
                return Mat();
            }
        }
    }
    return image;
}

void RGB8video::close() {
    m_f->close();
}

void RGB8video::getNewSizes(Scale how, int factor, int lines,int columns, int* new_lines, int* new_columns){
    switch (how) {
    case ReductionToMean:
    case ReductionToTopLeft:
        *new_lines=lines/factor;
        *new_columns=columns/factor;
        break;
    case Expansion:
        *new_lines=lines*factor;
        *new_columns=columns*factor;
        break;
    default:
        *new_lines = lines;
        *new_columns = columns;
        break;
    }
}

Mat RGB8video::scaleFrame(Mat orig, Scale how, int factor){
    int lines, columns;
    switch (how) {
    case ReductionToMean:
    case ReductionToTopLeft:
        lines=orig.rows/factor;
        columns=orig.cols/factor;
        break;
    case Expansion:
        lines=orig.rows*factor;
        columns=orig.cols*factor;
        break;
    default:
        lines = orig.rows;
        columns = orig.cols;
        break;
    }

    Mat image(Size(columns,lines),CV_8UC3);

    const unsigned char *mo;
    unsigned char *md;
    unsigned char** mos;

    switch (how) {
    case ReductionToMean:
        mos = new unsigned char*[factor];
        for(int i=0;i<orig.rows;i+=factor) {
            md = image.ptr<unsigned char>(i/factor);
            for (int m = 0; m < factor; m++){
                mos[m] = orig.ptr<unsigned char>(i+m);
            }
            for(int j=0;j<orig.cols*3;j+=factor*3) {
                int mean_r = 0, mean_g = 0, mean_b = 0, index = j/factor;
                for(int k = j; k < j+(3*factor); k+=3){
                    for (int m = 0; m < factor; m++){
                        mean_r += mos[m][k];
                        mean_g += mos[m][k+1];
                        mean_b += mos[m][k+2];
                    }
                }
                mean_r /= (factor*factor);
                mean_g /= (factor*factor);
                mean_b /= (factor*factor);
                md[index] = mean_r;
                md[index+1] = mean_g;
                md[index+2] = mean_b;
            }
        }
        break;
    case ReductionToTopLeft:
        for(int i=0;i<orig.rows;i+=factor) {
            mo = orig.ptr<const unsigned char>(i);
            md = image.ptr<unsigned char>(i/factor);
            for(int j=0;j<orig.cols*3;j+=factor*3) {
                for(int k = j,l=j/factor; k < j+3; k++,l++){
                    md[l]=mo[k];
                }
            }
        }
        break;
    case Expansion:
        for(int i=0;i<image.rows;i++) {
            mo = orig.ptr<const unsigned char>(i/factor);
            md = image.ptr<unsigned char>(i);
            for(int j=0;j<image.cols*3;j+=factor*3) {
                for(int k = j,l=j/factor; k < j+(3*factor); k+=3){
                    md[k]=mo[l];
                    md[k+1]=mo[l+1];
                    md[k+2]=mo[l+2];
                }
            }
        }
        break;
    default:
        return Mat();
    }
    return image;
}

YCbCrFrame::YCbCrFrame(Mat Y, Mat Cb, Mat Cr) : m_Y_lines(Y.rows),m_Y_columns(Y.cols) {
    m_Y = Y;
    m_Cb = Cb;
    m_Cr = Cr;
}

YCbCrFrame::YCbCrFrame(int lines,int columns) : m_Y_lines(lines),m_Y_columns(columns) {
    m_Y = Mat(Size(columns,lines),CV_8UC1);
    m_Cb = Mat(Size(columns/2,lines/2),CV_8UC1);
    m_Cr = Mat(Size(columns/2,lines/2),CV_8UC1);
}

YCbCrFrame::YCbCrFrame(const Mat& RGB8) : m_Y_lines(RGB8.rows),m_Y_columns(RGB8.cols) {

    m_Y = Mat(Size(m_Y_columns,m_Y_lines),CV_8UC1);
    m_Cb = Mat(Size(m_Y_columns/2,m_Y_lines/2),CV_8UC1);
    m_Cr = Mat(Size(m_Y_columns/2,m_Y_lines/2),CV_8UC1);

    double cb,cr;
    const unsigned char *mi, *mii;
    unsigned char r, g, b, *mi_Y, *mi_Cb, *mi_Cr;
    for(int i=0;i<m_Y_lines;i++) {
        mi = RGB8.ptr<const unsigned char>(i);
        mi_Y = m_Y.ptr<unsigned char>(i);

        if(i%2==0) {
            mii = RGB8.ptr<const unsigned char>(i+1);
            mi_Cb = m_Cb.ptr<unsigned char>(i/2);
            mi_Cr = m_Cr.ptr<unsigned char>(i/2);
        } else {
            mii = 0;
        }

        for(int j=0;j<m_Y_columns;j++) {
            r=mi[3*j];
            g=mi[3*j+1];
            b=mi[3*j+2];

            mi_Y[j] = 0.299*r+0.587*g+0.114*b;

            if (mii!=0 && j%2==0) {
                cb=0;
                cr=0;

                //top left
                cb += (-0.169*r) + (-0.331*g) + 0.499*b + 128;
                cr += (0.499*r) + (-0.418*g) + (-0.0813*b) + 128;

                //top right
                r=mi[3*j+3];
                g=mi[3*j+4];
                b=mi[3*j+5];
                cb += (-0.169*r) + (-0.331*g) + 0.499*b + 128;
                cr += (0.499*r) + (-0.418*g) + (-0.0813*b) + 128;

                //bottom left
                r=mii[3*j];
                g=mii[3*j+1];
                b=mii[3*j+2];
                cb += (-0.169*r) + (-0.331*g) + 0.499*b + 128;
                cr += (0.499*r) + (-0.418*g) + (-0.0813*b) + 128;

                //bottom right
                r=mii[3*j+3];
                g=mii[3*j+4];
                b=mii[3*j+5];
                cb += (-0.169*r) + (-0.331*g) + 0.499*b + 128;
                cr += (0.499*r) + (-0.418*g) + (-0.0813*b) + 128;

                // avg
                mi_Cb[j/2] = (unsigned char)round(cb/4);
                mi_Cr[j/2] = (unsigned char)round(cr/4);
            }
        }
    }
}

inline unsigned char YCbCrFrame::RGB8(double n) const {
    return (unsigned char) max(0.0, min(round(n), 255.0));
}

Mat YCbCrFrame::toRGB8() {
    Mat image(Size(m_Y_columns,m_Y_lines),CV_8UC3);

    unsigned char y, cb, cr, *mi, *mi_Y, *mi_Cb, *mi_Cr;
    for(int i=0;i<m_Y_lines;i++) {
        mi = image.ptr<unsigned char>(i);
        mi_Y = m_Y.ptr<unsigned char>(i);
        mi_Cb = m_Cb.ptr<unsigned char>(i/2);
        mi_Cr = m_Cr.ptr<unsigned char>(i/2);
        for(int j=0;j<m_Y_columns;j++) {
            y=mi_Y[j];
            cb=mi_Cb[j/2];
            cr=mi_Cr[j/2];

            mi[3*j]=RGB8((double)y+1.402*(cr-128));
            mi[3*j+1]=RGB8((double)y-0.344*(cb-128)-0.714*(cr-128));
            mi[3*j+2]=RGB8((double)y+1.772*(cb-128));
        }
    }

    return image;
}

YCbCrFrame YCbCrFrame::dpcmEncodeLossy(DpcmSelection how,float lY,float lCb,float lCr){
    Mat Y = m_Y / lY;
    Mat Cb = m_Cb / lCb;
    Mat Cr = m_Cr / lCr;
    YCbCrFrame temp = YCbCrFrame(Y,Cb,Cr);
    YCbCrFrame frame = temp.dpcmEncode(how);
//    *frame.Y() = *frame.Y() / lY;
//    *frame.Cb() = *frame.Cb() / lCb;
//    *frame.Cr() = *frame.Cr() / lCr;
    return frame;
}

YCbCrFrame YCbCrFrame::dpcmDecodeLossy(DpcmSelection how,float lY,float lCb,float lCr){
//    Mat Y = m_Y * lY;
//    Mat Cb = m_Cb * lCb;
//    Mat Cr = m_Cr * lCr;
//    YCbCrFrame temp = YCbCrFrame(Y,Cb,Cr);
//    YCbCrFrame frame = temp.dpcmDecode(how);
    YCbCrFrame frame = dpcmDecode(how);
    *frame.Y() = *frame.Y() * lY;
    *frame.Cb() = *frame.Cb() * lCb;
    *frame.Cr() = *frame.Cr() * lCr;
    return frame;
}

YCbCrFrame YCbCrFrame::dpcmEncode(DpcmSelection how) {
    Mat dpcm_Y(Size(m_Y_columns,m_Y_lines),CV_32S);
    Mat dpcm_Cb(Size(m_Y_columns/2,m_Y_lines/2),CV_32S);
    Mat dpcm_Cr(Size(m_Y_columns/2,m_Y_lines/2),CV_32S);

    dpcm(how,m_Y_lines,m_Y_columns,*Y(),&dpcm_Y);
    dpcm(how,m_Y_lines/2,m_Y_columns/2,*Cb(),&dpcm_Cb);
    dpcm(how,m_Y_lines/2,m_Y_columns/2,*Cr(),&dpcm_Cr);

    YCbCrFrame encoded(dpcm_Y, dpcm_Cb, dpcm_Cr);

    return encoded;
}

YCbCrFrame YCbCrFrame::dpcmDecode(DpcmSelection how) {
    Mat rev_dpcm_Y(Size(m_Y_columns,m_Y_lines),CV_8UC1);
    Mat rev_dpcm_Cb(Size(m_Y_columns/2,m_Y_lines/2),CV_8UC1);
    Mat rev_dpcm_Cr(Size(m_Y_columns/2,m_Y_lines/2),CV_8UC1);

    reverseDpcm(how,m_Y_lines,m_Y_columns,*Y(),&rev_dpcm_Y);
    reverseDpcm(how,m_Y_lines/2,m_Y_columns/2,*Cb(),&rev_dpcm_Cb);
    reverseDpcm(how,m_Y_lines/2,m_Y_columns/2,*Cr(),&rev_dpcm_Cr);

    YCbCrFrame decoded(rev_dpcm_Y, rev_dpcm_Cb, rev_dpcm_Cr);

    return decoded;
}

inline void YCbCrFrame::dpcm(DpcmSelection how,int lines, int columns, const Mat& orig, Mat* dest) const {
    const unsigned char *mo,*mop;
    int *md;
    unsigned char n,w,nw; //north,west,north-west
    for(int i=0;i<lines;i++) {
        mo = orig.ptr<const unsigned char>(i);
        md = dest->ptr<int>(i);
        for(int j=0;j<columns;j++) {

            //
            // on top and left (not i==0&&j==0) limits we ALWAYS use west and north prediction, UNLESS v0 is selected
            //
            if ( (i==0 && j==0) || how==v0) {
                md[j]=mo[j];
            } else if (i==0) { // always west
                w=mo[j-1];
                md[j]=mo[j]-dpcmPrediction(v1_w,0,w,0);
            } else if (j==0) { // always north
                mop = orig.ptr<const unsigned char>(i-1);
                n=mop[j];
                md[j]=mo[j]-dpcmPrediction(v2_n,n,0,0);
            } else {
                mop = orig.ptr<const unsigned char>(i-1);
                n=mop[j];
                w=mo[j-1];
                nw=mop[j-1];
                md[j]=mo[j]-dpcmPrediction(how,n,w,nw);
            }

            /*if (i==1 && j==1) {
                printf("E -> VAL: %d   n: %d    w: %d   p: %d    s: %d\n",mo[j],n,w,dpcmPrediction(how,n,w,nw),md[j]);
            }*/
        }
    }
}


inline void YCbCrFrame::reverseDpcm(DpcmSelection how,int lines, int columns, const Mat& orig, Mat* dest) const {
    const int *mo;
    unsigned char *md,*mdp;
    int n,w,nw; //north,west,north-west
    for(int i=0;i<lines;i++) {
        mo = orig.ptr<const int>(i);
        md = dest->ptr<unsigned char>(i);
        for(int j=0;j<columns;j++) {

            // see notes on dpcm(...)
            if ( (i==0 && j==0) || how==v0) {
                md[j]=mo[j];
            } else if (i==0) {
                w=md[j-1];
                md[j]=dpcmPrediction(v1_w,0,w,0)+mo[j];
            } else if (j==0) {
                mdp = dest->ptr<unsigned char>(i-1);
                n=mdp[j];
                md[j]=dpcmPrediction(v2_n,n,0,0)+mo[j];
            } else {
                mdp = dest->ptr<unsigned char>(i-1);
                n=mdp[j];
                w=md[j-1];
                nw=mdp[j-1];
                md[j]=dpcmPrediction(how,n,w,nw)+mo[j];
            }
            /*if (i==1 && j==1) {
                printf("  D -> s: %d   n: %d    w: %d   p: %d    VAL: %d\n",mo[j],n,w,dpcmPrediction(how,n,w,nw),md[j]);
            }*/
        }
    }
}

inline int YCbCrFrame::dpcmPrediction(DpcmSelection how, int n, int w, int nw) const {
    switch (how) {
    case v0:
        return 0;
    case v1_w:
        return w;
    case v2_n:
        return n;
    case v3:
        return nw;
    case v4:
        return n+w-nw;
    case v5:
        return n+(w-nw)/2;
    case v6:
        return w+(n-nw)/2;
    case v7:
        return (n+w)/2;
    }

    return 0/*and no warning*/;
}

YCbCrFrame& YCbCrFrame::operator=(const YCbCrFrame& other){
    if(this == &other) {
        return *this;
    }
    m_Y_lines = other.m_Y_lines;
    m_Y_columns = other.m_Y_columns;
    m_Y = other.m_Y;
    m_Cr = other.m_Cr;
    m_Cb = other.m_Cb;
    return *this;
}

CodedFrame::CodedFrame(int rows,int cols){
    m_Y = Mat(Size(cols,rows),CV_32S);
    m_Cb = Mat(Size(cols/2,rows/2),CV_32S);
    m_Cr = Mat(Size(cols/2,rows/2),CV_32S);
}

CodedFrame::CodedFrame(YCbCrFrame frame){
    m_Y = frame.Y()->clone();
    m_Cb = frame.Cb()->clone();
    m_Cr = frame.Cr()->clone();
}

CodedFrame::CodedFrame(Mat Y, Mat Cb, Mat Cr){
    m_Y = Y;
    m_Cb = Cb;
    m_Cr = Cr;
}

YCbCrFrame CodedFrame::getYCbCrFrame(){
    YCbCrFrame yuvframe(m_Y, m_Cb, m_Cr);
    return yuvframe;
}

CodedFrame& CodedFrame::operator=(const CodedFrame& other){
    if(this == &other) {
        return *this;
    }
    m_Y = other.m_Y;
    m_Cr = other.m_Cr;
    m_Cb = other.m_Cb;
    m_type = other.m_type;
    m_block_rows = other.m_block_rows;
    m_block_cols = other.m_block_cols;
    return *this;
}

CodedFrame MotionCompensation::getCodedFrame(YCbCrFrame& frame){
    CodedFrame coded_frame(frame.Ylines(), frame.Ycolumns());
    double threshold = m_threshold;
    if(m_current_frame == 0){
        if(m_lossy){
            coded_frame = CodedFrame(frame.dpcmEncodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr));
            YCbCrFrame tempYUV = coded_frame.getYCbCrFrame().dpcmDecodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr);
            setIframe(tempYUV);
        }else{
            coded_frame = CodedFrame(frame.dpcmEncode(m_dpcm_op));
            setIframe(frame);
        }
        coded_frame.setType(Iframe);
        //cout << " - Setting frame " << m_current_frame << " as Iframe. FIRST!"<< endl;
        m_current_frame++;
        return coded_frame;
    }
    CodedFrame temp;
    double mean = processFrame(frame, &coded_frame);
    if(m_threshold == -1){
        if(m_lossy){
            temp = CodedFrame(frame.dpcmEncodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr));
        }else{
            temp = CodedFrame(frame.dpcmEncode(m_dpcm_op));
        }
        threshold = getMean(temp.Y(),temp.Cb(), temp.Cr());
    }

    if (mean > threshold){
        if(m_threshold == -1){
            coded_frame = temp;
            if(m_lossy){
                YCbCrFrame tempYUV = temp.getYCbCrFrame().dpcmDecodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr);
                setIframe(tempYUV);
            }else{
                setIframe(frame);
            }
        }else{
            if(m_lossy){
                coded_frame = CodedFrame(frame.dpcmEncodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr));
                YCbCrFrame tempYUV = coded_frame.getYCbCrFrame().dpcmDecodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr);
                setIframe(tempYUV);
            }else{
                coded_frame = CodedFrame(frame.dpcmEncode(m_dpcm_op));
                setIframe(frame);
            }
        }
        coded_frame.setType(Iframe);
        //cout << " - Setting frame " << m_current_frame << " as Iframe. Mean = "<< mean << " threshold = "<< threshold << "  -- I" << endl;
        m_current_frame++;
        return coded_frame;
    }
    coded_frame.setType(Pframe);
    if(m_lossy){
        *coded_frame.Y() = *coded_frame.Y() / m_lY;
        *coded_frame.Cb() = *coded_frame.Cb() / m_lCb;
        *coded_frame.Cr() = *coded_frame.Cr() / m_lCr;
    }
    //cout << " - Setting frame " << m_current_frame << " as Pframe. Mean = "<< mean << " threshold = "<< threshold << endl;
    m_current_frame++;
    return coded_frame;
}

YCbCrFrame MotionCompensation::getOriginalFrame(CodedFrame& coded_frame){
    YCbCrFrame frame(coded_frame.Y()->rows, coded_frame.Y()->cols);
    if(coded_frame.getType() == Iframe){
        if(m_lossy){
            frame = coded_frame.getYCbCrFrame().dpcmDecodeLossy(m_dpcm_op,m_lY,m_lCb,m_lCr);
        }else{
            frame = coded_frame.getYCbCrFrame().dpcmDecode(m_dpcm_op);
        }
        setIframe(frame);
        m_current_frame++;
        return frame;
    }
    if(m_lossy){
        *coded_frame.Y() = *coded_frame.Y() * m_lY;
        *coded_frame.Cb() = *coded_frame.Cb() * m_lCb;
        *coded_frame.Cr() = *coded_frame.Cr() * m_lCr;
    }
    processCodedFrame(coded_frame, &frame);
    m_current_frame++;
    return frame;
}

void MotionCompensation::processCodedFrame(CodedFrame coded, YCbCrFrame*frame){
    vector<int> row_locs = coded.getRowLocations();
    vector<int> col_locs = coded.getColLocations();

    int rows, cols, row_i=0,col_i=0,half_block = ceil(1.0*m_block_size/2);
    int total_row_yblocks = coded.Y()->rows / m_block_size;
    int total_col_yblocks = coded.Y()->cols / m_block_size;
    int total_row_cblocks = coded.Cb()->rows / half_block;
    int total_col_cblocks = coded.Cb()->cols / half_block;
    int y_row_res = coded.Y()->rows % m_block_size;
    int y_col_res = coded.Y()->cols % m_block_size;
    int c_row_res = coded.Cb()->rows % half_block;
    int c_col_res = coded.Cb()->cols % half_block;

    for(int i = 0, ii=0; i < coded.Y()->rows; i+=m_block_size, ii+=half_block){
        for(int j = 0,jj=0; j < coded.Y()->cols; j+=m_block_size, jj+=half_block){
            rows = cols = m_block_size;
            if(j == total_col_yblocks*m_block_size && y_col_res != 0){
                cols = y_col_res;
            }
            if(i == total_row_yblocks*m_block_size && y_row_res != 0){
                rows = y_row_res;
            }

            buildBlock(Mat(*m_Iframe.Y(),Rect(col_locs[col_i++],row_locs[row_i++],cols,rows)),
                    Mat(*coded.Y(), Rect(j,i,cols,rows)),
                    frame->Y(),i,j);

            if(jj>=coded.Cb()->cols || ii >= coded.Cb()->rows){
                continue;
            }

            rows = cols = half_block;
            if(jj == total_col_cblocks*half_block && c_col_res != 0){
                cols = c_col_res;
            }
            if(ii == total_row_cblocks*half_block && c_row_res != 0){
                rows = c_row_res;
            }

            buildBlock(Mat(*m_Iframe.Cb(),Rect(col_locs[col_i++],row_locs[row_i++],cols,rows)),
                    Mat(*coded.Cb(), Rect(jj,ii,cols,rows)),
                    frame->Cb(),ii,jj);

            buildBlock(Mat(*m_Iframe.Cr(),Rect(col_locs[col_i++],row_locs[row_i++],cols,rows)),
                    Mat(*coded.Cr(), Rect(jj,ii,cols,rows)),
                    frame->Cr(),ii,jj);
        }
    }
}

double MotionCompensation::getMean(Mat*Y,Mat*Cb,Mat*Cr){
    double meanY=0,meanCb=0,meanCr=0;
    const int *mY,*mCb,*mCr;
    for(int i = 0; i < Y->rows; i++){
        mY = Y->ptr<const int>(i);
        if(i%2 == 0){
            mCb = Cb->ptr<const int>(i/2);
            mCr = Cr->ptr<const int>(i/2);
        }
        for(int j = 0; j < Y->cols; j++){
            meanY += abs(mY[j]);
            if(i%2==0 && j%2==0){
                meanCb += abs(mCb[j/2]);
                meanCr += abs(mCr[j/2]);
            }
        }
    }
    meanY /= (Y->rows*Y->cols);
    meanCb /= (Cb->rows*Cb->cols);
    meanCr /= (Cr->rows*Cr->cols);
    return (meanY*0.6666+meanCb*0.1666+meanCr*0.1666);
}

void MotionCompensation::printMat(Mat*mat){
    const unsigned char *m;
    for(int i = 0; i < mat->rows; i++){
        m = mat->ptr<const unsigned char>(i);
        for(int j =0; j < mat->cols; j++){
            printf("%12d ",(int)m[j]);
        }
        printf("\n");
    }
    printf("\n");
}

void MotionCompensation::printMatCoded(Mat*mat){
    const int *m;
    for(int i = 0; i < mat->rows; i++){
        m = mat->ptr<const int>(i);
        for(int j =0; j < mat->cols; j++){
            printf("%4d ",m[j]);
        }
        printf("\n");
    }
    printf("\n");
}

void MotionCompensation::buildBlock(const Mat iframe,const Mat diffs,Mat*block_mat, int row, int col){
    const unsigned char *mi;
    const int *md;
    unsigned char *mb;
    for (int i = row, ii=0; ii < diffs.rows; i++,ii++){
        mi = iframe.ptr<const unsigned char>(ii);
        md = diffs.ptr<const int>(ii);
        mb = block_mat->ptr<unsigned char>(i);
        for (int j = col, jj=0; jj < diffs.cols; j++,jj++){
            mb[j] = mi[jj] + md[jj];
        }
    }
}

double MotionCompensation::processFrame(YCbCrFrame o_frame,CodedFrame* d_frame){
    double meanY=0,meanCb=0,meanCr=0,tmp_mean;
    int nYblocks = 0,nCblocks=0,half_block = ceil(1.0*m_block_size/2);
    int rows, cols,tmp_row, tmp_col;
    vector<int> row_locations_vector;
    vector<int> col_locations_vector;

    int total_row_yblocks = o_frame.Ylines() / m_block_size;
    int total_col_yblocks = o_frame.Ycolumns() / m_block_size;
    int total_row_cblocks = o_frame.Clines() / half_block;
    int total_col_cblocks = o_frame.Ccolumns() / half_block;
    int y_row_res = o_frame.Ylines() % m_block_size;
    int y_col_res = o_frame.Ycolumns() % m_block_size;
    int c_row_res = o_frame.Clines() % half_block;
    int c_col_res = o_frame.Ccolumns() % half_block;
    Mat tmp_block;

    for(int i = 0, ii=0; i < o_frame.Ylines(); i+=m_block_size, ii+=half_block){
        for(int j = 0,jj=0; j < o_frame.Ycolumns(); j+=m_block_size, jj+=half_block){
            rows = cols = m_block_size;
            if(j == total_col_yblocks*m_block_size && y_col_res != 0){
                cols = y_col_res;
            }
            if(i == total_row_yblocks*m_block_size && y_row_res != 0){
                rows = y_row_res;
            }
            getBestBlock(m_Iframe.Y(), Mat(*o_frame.Y(), Rect(j,i,cols,rows)),i,j,&tmp_block,&tmp_mean,&tmp_row,&tmp_col);
            copyBlockToMatrix(&tmp_block, d_frame->Y(),i,j);
            meanY += m_lossy ? tmp_mean / m_lY : tmp_mean;
            row_locations_vector.push_back(tmp_row);
            col_locations_vector.push_back(tmp_col);

            nYblocks++;

            if(jj>=o_frame.Cb()->cols || ii >= o_frame.Cb()->rows){
                continue;
            }

            rows = cols = half_block;
            if(jj == total_col_cblocks*half_block && c_col_res != 0){
                cols = c_col_res;
            }
            if(ii == total_row_cblocks*half_block && c_row_res != 0){
                rows = c_row_res;
            }

            getBestBlock(m_Iframe.Cb(), Mat(*o_frame.Cb(), Rect(jj,ii,cols,rows)),ii,jj,&tmp_block,&tmp_mean,&tmp_row,&tmp_col);
            copyBlockToMatrix(&tmp_block, d_frame->Cb(),ii,jj);
            meanCb += m_lossy ? tmp_mean / m_lCb : tmp_mean;
            row_locations_vector.push_back(tmp_row);
            col_locations_vector.push_back(tmp_col);

            getBestBlock(m_Iframe.Cr(), Mat(*o_frame.Cr(), Rect(jj,ii,cols,rows)),ii,jj,&tmp_block,&tmp_mean,&tmp_row,&tmp_col);
            copyBlockToMatrix(&tmp_block, d_frame->Cr(),ii,jj);
            meanCr += m_lossy ? tmp_mean / m_lCr : tmp_mean;
            row_locations_vector.push_back(tmp_row);
            col_locations_vector.push_back(tmp_col);

            nCblocks++;
        }
    }
    meanY /= nYblocks;
    meanCb /= nCblocks;
    meanCr /= nCblocks;
    d_frame->setRowLocations(row_locations_vector);
    d_frame->setColLocations(col_locations_vector);
    return (meanY*0.6666+meanCb*0.1666+meanCr*0.1666);
}

void MotionCompensation::copyBlockToMatrix(Mat*block, Mat*mat,int row, int col){
    const int *mb;
    int *mm;
    for(int i = row, ii=0; ii < block->rows; i++,ii++){
        mb = block->ptr<const int>(ii);
        mm = mat->ptr<int>(i);
        for(int j = col, jj=0; jj < block->cols; j++,jj++){
            mm[j] = mb[jj];
        }
    }
}

void MotionCompensation::getBestBlock(Mat*iframe,const Mat current_block, int row, int col,Mat*block,double*mean,int*loc_row,int*loc_col){
    int start_row = (row - m_search_area) < 0 ? 0 : row - m_search_area;
    int start_col = (col - m_search_area) < 0 ? 0 : col - m_search_area;
    int stop_row = (row + m_search_area) > (iframe->rows - current_block.rows) ?
                (iframe->rows - current_block.rows)+1 : row + m_search_area + 1;
    int stop_col = (col + m_search_area) > (iframe->cols - current_block.cols) ?
                (iframe->cols - current_block.cols)+1 : col + m_search_area + 1;

    Mat tmp_block = newBlock(current_block.rows, current_block.cols);
    double tmp_mean;
    *mean = numeric_limits<int>::max();
    for(int i = start_row; i < stop_row; i++){
        for(int j = start_col; j < stop_col; j++){
            getBlockWithDifferences(iframe,i,j,current_block,&tmp_block,&tmp_mean);
            if(tmp_mean < *mean){
                *mean = tmp_mean;
                *block = tmp_block.clone();
                *loc_row = i;
                *loc_col = j;
                if(tmp_mean == 0){ //thats perfect! Stop here! (but probably never happen)
                    return;
                }
            }
        }
    }
}

void MotionCompensation::getBlockWithDifferences(Mat*iframe, int row,int col,const Mat original_block,Mat*block,double*mean){
    int end_row = row + original_block.rows;
    int end_col = col + original_block.cols;

    const unsigned char *mp;
    const unsigned char *mi;
    int *md;
    *mean = 0;
    for(int oi=row, di = 0; oi < end_row; oi++, di++) {
        mi = iframe->ptr<const unsigned char>(oi);
        mp = original_block.ptr<const unsigned char>(di);
        md = block->ptr<int>(di);
        for(int oj=col, dj = 0; oj < end_col; oj++, dj++) {
            md[dj] = (mp[dj] - mi[oj]);
            *mean += abs(md[dj]);
        }
    }
    *mean /= original_block.rows*original_block.cols;
}

Mat MotionCompensation::newBlock(int rows,int cols){
    return Mat(Size(cols,rows),CV_32S);
}

} //CAV
