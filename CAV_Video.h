#ifndef T3_CAVRGB_H
#define T3_CAVRGB_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

namespace Cav {
enum Scale {
    Expansion,
    ReductionToTopLeft,
    ReductionToMean
};

enum DpcmSelection {
    // same as Lossless JPEG
    v0 = 0,
    v1_w = 1,
    v2_n = 2,
    v3 = 3,
    v4 = 4,
    v5 = 5,
    v6 = 6,
    v7 = 7
};

enum FrameType {
    Pframe = 0,
    Iframe = 1,
};

class RGB8video {
public:
    RGB8video() : m_file_pos(0) {}
    RGB8video(char* file) : m_file_pos(0) {
        setFile(file, false);
    }
    RGB8video(char* file, bool write) : m_file_pos(0) {
        setFile(file, write);
    }

    ~RGB8video() {
        delete m_f;
    }
    void setFile(char*,bool);
    bool is_open() const;
    bool read_header();
    bool write_header(int,int);
    bool writeFrame(Mat);
    int lines() const { return m_lines; }
    int columns() const { return m_columns; }
    int frames() const;
    bool hasNextFrame() const;
    Mat nextFrame();
    void close();

    static void getNewSizes(Scale, int, int, int, int*, int*);
    static Mat scaleFrame(Mat,Scale, int);

private:
    fstream* m_f;
    int m_file_size;
    int m_data_size;
    int m_file_pos;
    int m_lines;
    int m_columns;
};

class YCbCrFrame {
public:
    YCbCrFrame() : m_Y_lines(0),m_Y_columns(0){}
    YCbCrFrame(int,int);
    YCbCrFrame(const Mat&);
    YCbCrFrame(Mat,Mat,Mat);

    Mat* Y() { return &m_Y; }
    Mat* Cb() { return &m_Cb; }
    Mat* Cr() { return &m_Cr; }

    int Ylines() const {return m_Y_lines;}
    int Ycolumns() const {return m_Y_columns;}

    int Clines() const {return m_Y_lines/2;}
    int Ccolumns() const {return m_Y_columns/2;}

    Mat toRGB8();

    YCbCrFrame dpcmEncode(DpcmSelection);
    YCbCrFrame dpcmEncodeLossy(DpcmSelection,float,float,float);
    YCbCrFrame dpcmDecode(DpcmSelection);
    YCbCrFrame dpcmDecodeLossy(DpcmSelection,float,float,float);

    YCbCrFrame& operator=(const YCbCrFrame&);

private:
    int m_Y_lines;
    int m_Y_columns;
    Mat m_Y;
    Mat m_Cr;
    Mat m_Cb;
    int m_mean;

    unsigned char RGB8(double) const;

    void dpcm(DpcmSelection,int, int, const Mat&, Mat*) const;
    void reverseDpcm(DpcmSelection,int, int, const Mat&, Mat*) const;

    int dpcmPrediction(DpcmSelection, int, int, int) const;
};

class CodedFrame {
public:
    CodedFrame(){}
    CodedFrame(int,int);
    CodedFrame(YCbCrFrame);
    CodedFrame(Mat,Mat,Mat);

    Mat* Y() { return &m_Y; }
    Mat* Cb() { return &m_Cb; }
    Mat* Cr() { return &m_Cr; }

    YCbCrFrame getYCbCrFrame();

    CodedFrame& operator=(const CodedFrame&);

    void setType(FrameType type){m_type=type;}
    FrameType getType(){return m_type;}
    void setRowLocations(vector<int> rows){m_block_rows = rows;}
    void setColLocations(vector<int> cols){m_block_cols = cols;}
    vector<int> getRowLocations(){return m_block_rows;}
    vector<int> getColLocations(){return m_block_cols;}

private:
    Mat m_Y;
    Mat m_Cr;
    Mat m_Cb;

    FrameType m_type;
    vector<int> m_block_rows;
    vector<int> m_block_cols;
};

class MotionCompensation {
public:
    MotionCompensation(int search_area, int block_size, int threshold, DpcmSelection dpcm_op)
        : m_block_size(block_size),
          m_threshold(threshold),
          m_dpcm_op(dpcm_op),
          m_current_frame(0),
          m_search_area(search_area),
          m_lossy(false){}

    MotionCompensation(int block_size, DpcmSelection dpcm_op)
        : m_block_size(block_size),
          m_threshold(0),
          m_dpcm_op(dpcm_op),
          m_current_frame(0),
          m_search_area(0),
          m_lossy(false){}

    CodedFrame getCodedFrame(YCbCrFrame&);
    YCbCrFrame getOriginalFrame(CodedFrame&);
    void setLossy(float lY,float lCb,float lCr){
        m_lossy = true;
        m_lY = lY;
        m_lCb = lCb;
        m_lCr = lCr;
    }

    //Testing purposes
    static void printMat(Mat*);
    static void printMatCoded(Mat*);


private:
    const int m_block_size;
    int m_threshold;
    const DpcmSelection m_dpcm_op;
    int m_current_frame;
    const int m_search_area;
    bool m_lossy;
    float m_lY,m_lCb,m_lCr;

    YCbCrFrame m_Iframe;

    double getMean(Mat*,Mat*,Mat*);
    void setIframe(YCbCrFrame& frame) { m_Iframe = frame;}
    double processFrame(YCbCrFrame,CodedFrame*);
    void buildBlock(const Mat,const Mat,Mat*,int,int);
    void processCodedFrame(CodedFrame,YCbCrFrame*);
    void getBestBlock(Mat*,const Mat,int,int,Mat*,double*,int*,int*);
    void getBlockWithDifferences(Mat*,int,int,const Mat,Mat*,double*);
    void copyBlockToMatrix(Mat*,Mat*,int, int);
    Mat newBlock(int,int);
};
}

#endif //T3_CAVRGB_H


// GARBAGE

/*
    // octave:1> dctmtx(8)
    static double DCT_8x8[8][8] =
    {
        { 0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553, 0.353553 },
        { 0.490393, 0.415735, 0.277785, 0.097545, -0.097545, -0.277785, -0.415735, -0.490393 },
        { 0.461940, 0.191342, -0.191342, -0.461940, -0.461940, -0.191342, 0.191342, 0.461940 },
        { 0.415735, -0.097545, -0.490393, -0.277785, 0.277785, 0.490393, 0.097545, -0.415735 },
        { 0.353553, -0.353553, -0.353553, 0.353553, 0.353553, -0.353553, -0.353553, 0.353553 },
        { 0.277785, -0.490393, 0.097545, 0.415735, -0.415735, -0.097545, 0.490393, -0.277785 },
        { 0.191342, -0.461940, 0.461940, -0.191342, -0.191342, 0.461940, -0.461940, 0.191342 },
        { 0.097545, -0.277785, 0.415735, -0.490393, 0.490393, -0.415735, 0.277785, -0.097545 }
    };
    static const Mat DCT_8x8_Mat = Mat(8, 8, CV_32F, DCT_8x8 );
    static const Mat DCT_8x8_Mat_T = DCT_8x8_Mat.t();

    YCbCrFrame dct() const;

    Mat dctBlock8x8(const Mat&) const;

    YCbCrFrame YCbCrFrame::dct() const {
        Mat dct_Y(Size(Ylines(),Ycolumns()),CV_32F);
        Mat dct_Cb(Size(Clines(),Ccolumns()),CV_32F);
        Mat dct_Cr(Size(Clines(),Ccolumns()),CV_32F);

        unsigned char *mi;
        double *md;
        Mat dest(Size(8,8),CV_32F);

        for(int i=0;i<lines;i+=8) {
            for(int j=0;j<columns;j+=8) {
                copySquare(i,j,8,Y(),dest,-127);
            }
        }
    }

    inline Mat YCbCrFrame::dctBlock8x8(const Mat& block) const {
    return DCT_8x8_Mat*block*DCT_8x8_Mat_T;
    / *
    double *rr;
    for(int i=0;i<8;i++) {
        rr = r.ptr<double>(i);
        for(int j=0;j<8;j++) {
            printf("%3.2f ",rr[j]);
        }
        printf("\n");
    }
    * /
}
*/
