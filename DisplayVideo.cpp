#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/opencv.hpp>
#include "CAV_Video.h"

using namespace cv;
using namespace std;
using namespace Cav;

int readDev();
int readFile(char* file);

int main(int argc, char** argv ) {

    if (argc == 2) {
        return readFile(argv[1]);
    }

    return readDev();
}

int readDev() {
    Mat image;
    VideoCapture inputVideo(0);              // Open input

    if (!inputVideo.isOpened()) {
        std::cout << "Could not open camera: " << std::endl;
        return -1;
    }

    for (;; ) { //Show the image captured in the window and repeat
        inputVideo >> image;                    // read
        if (image.empty()) break;               // check if at end

        imshow("Display Image", image);

        if (waitKey(5) >= 0) break;
    }
    return 0;
}

int readFile(char* file) {
    printf("%s\n",file);

    RGB8video cav(file);

    if (!cav.is_open())  {
        return -1;
    }

    if (!cav.read_header())  {
        return -2;
    }

    Mat image;
    int search_area = 3;
    int block_size = 4;
    int threshold = -1;
    float factor = 4;
    DpcmSelection dpcm = v5;
    MotionCompensation mc(search_area, block_size, threshold, dpcm);
    MotionCompensation mc2( block_size, dpcm);
    mc.setLossy(factor,factor,factor);
    mc2.setLossy(factor,factor,factor);
    int i = 0;
    cout << "Rows: " << cav.lines() << " Columns: " << cav.columns() << endl;
    while (cav.hasNextFrame()) {
        i++;
        image = cav.nextFrame();

        if (image.empty()) break;



        YCbCrFrame frame(image);

        //imshow("Display Image", Mat(image, Rect(20,20,100,100)));
        //waitKey(0);

        CodedFrame f = mc.getCodedFrame(frame);

        image = mc2.getOriginalFrame(f).toRGB8();
        imshow("Display Image", image);
        //if (waitKey(5) >= 0) break;
        waitKey(0);
        if (i==2) break;
        //
        //  Test conversion
        //
        //double e,psnr;
        //YCbCrFrame(image, cav.lines(), cav.columns()).compare(image,&e,&psnr);
        //cerr << e << endl << psnr << endl << endl;
        //(void)e;
        //(void)psnr;

        //int lines, columns;
        //RGB8video::getNewSizes(ReductionToTopLeft, 2,cav.lines(),cav.columns(), &lines,&columns);

//        image = YCbCrFrame(image, cav.lines(), cav.columns())
//                .scale(ReductionToTopLeft,2,lines,columns)
//                .dpcmEncode(v5)
//                .dpcmDecode(v5)
//                .toRGB8();


        //
        //
        //

        //imshow("Display Image", image);
        //if (waitKey(5) >= 0) break;
    }
    cav.close();

    return 0;
}
