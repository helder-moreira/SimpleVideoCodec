#include "histogramwidget.h"

#include <QPainter>
#include <QColor>
#include <cmath>

HistogramWidget::HistogramWidget(QWidget* parent, Qt::WindowFlags f) : QWidget(parent, f),
m_height_max(1),
m_force_height_max(0),
m_width(1),
m_sampling(0)
{}

void HistogramWidget::setData(const QList<QVector<int> >& data) {
    m_data = data;
    while (m_visibility.length()<m_data.length()) { //make visible
        m_visibility.append(true);
    }

    while (m_labels.length()<m_data.length()) { //give a name
        m_labels.append(tr("Dataset %1").arg(m_labels.length()+1));
    }

    setFocus();
    update();
}

void HistogramWidget::setLabels(const QList<QString>& labels) {
    m_labels = labels;
    update();
}

void HistogramWidget::setColors(const QList<QColor>& colors) {
    m_colors = colors;
    update();
}

void HistogramWidget::setVisiblity(const QList<bool>& visibility) {
    m_visibility = visibility;
    update();
}

QList<QVector<int> > HistogramWidget::data() const {
    return m_data;
}

QList<QString> HistogramWidget::labels() const {
    return m_labels;
}

QList<QColor> HistogramWidget::colors() const {
    return m_colors;
}

QList<bool> HistogramWidget::visiblity() const {
    return m_visibility;
}

void HistogramWidget::clear() {
    m_data.clear();
    m_colors.clear();
    m_visibility.clear();
    update();
}

void  HistogramWidget::keyPressEvent(QKeyEvent *event){
    int u=false;
    if (event->key()==Qt::Key_Up) {
        int z=(m_force_height_max==0?m_height_max:m_force_height_max)/1.5;
        if (z!=m_force_height_max){
            m_force_height_max=z;
            u=true;
        }
    } else if (event->key()==Qt::Key_Down) {
        int z=(m_force_height_max==0?m_height_max:m_force_height_max)*1.5;
        z=qMax(0,z); //do not overflow
        if (z!=m_force_height_max){
            m_force_height_max=z;
            u=true;
        }
    } else if (event->key()==Qt::Key_Backspace) {
        int z=0;
        if (z!=m_force_height_max){
            m_force_height_max=z;
            u=true;
        }
    } else if (event->key()==Qt::Key_0) { //0 -> make all visible
        for (int i=0;i<m_visibility.size();i++) {
            if (!m_visibility[i]){
                m_visibility[i]=true;
                u=true;
            }
        }
    } else if (event->key()>=49 && event->key()<=57) { // 1 - 9
        int i=event->key()-49;
        if (i<m_visibility.length()) {
            m_visibility[i]=!m_visibility[i];
            u=true;
        }
    } // else ... more than 9 datasets... use letters?!?!
    else if (event->key() == Qt::Key_S) {
        m_sampling = !m_sampling;
        u=true;
    }
    
    if (u) {
        update();
    }
}

void HistogramWidget::paintEvent(QPaintEvent*) {
    QRect viewPort = rect();
    int xLeft = viewPort.left();
    int xRight = viewPort.right();
    int yTop = viewPort.top();
    int yBottom = viewPort.bottom();
    int width = viewPort.width();
    int height = viewPort.height();
     
    QPainter painter(this);

    QPen pen;
    pen.setColor(Qt::black);
    pen.setWidth(1);
    painter.setPen(pen);

    QBrush brush(Qt::SolidPattern);
    brush.setColor("#DDDDDD");
    painter.setBrush(brush);

    //gray background
    painter.drawRect(xLeft, yTop, xRight, yBottom);

    //vertical lines
    pen.setColor("#AAAAAA");
    pen.setStyle(Qt::DashDotLine);
    painter.setPen(pen);

    int stepsV = 1<< int(log(width/40.0f)/log(2.0f));
    for(int i=1; i<stepsV; ++i)
    {
        painter.drawLine(width*float(i)/stepsV, yTop+1,
                         width*float(i)/stepsV, yBottom-1);
    }

    //horizontal lines
    int stepsH = 1<< int(log(height/40.0f)/log(2.0f));
    for(int i=1; i<stepsH; ++i)
    {
        painter.drawLine(xLeft+1, height*float(i)/stepsH,
                         xRight-1,height*float(i)/stepsH);
    }

    //Histogram
    int data_set_size = m_data[0].size();

    pen.setColor("#016790");
    painter.setPen(pen);
    if( !data_set_size ) {
        painter.drawText(xLeft+2, yBottom-2, tr("Histogram off"));
        return;
    }

    // find maximum height in data set unit
    int heightMax=1;
    if (m_force_height_max==0) {
        for (int j=0;j<m_data.length();j++) {
            for( int i=0; i<data_set_size; ++i )
                if( m_data[j][i]>heightMax ) heightMax = m_data[j][i];
        }
    } else {
        heightMax=m_force_height_max;
    }

    painter.drawText(xLeft+2, yBottom-2, tr("scale [%1, %2]").arg(data_set_size/stepsV).arg(heightMax/stepsH));

    // do not update heightmax if variation <5%
    if( abs(m_height_max-heightMax)/float(m_height_max) > 0.05f )
        m_height_max = heightMax;
    
    // scale histogram from data set unit to pixels unit
    QPolygon myPolygon;
    pen.setStyle(Qt::SolidLine);
    pen.setWidth(m_width);

    float wScale;
    float hScale;
    int samples;

    if( data_set_size < width ) {
        wScale = width/float(data_set_size);
        hScale = height/float(m_height_max);
        samples = data_set_size;
    } else {
        wScale = float(data_set_size-1)/(width-1);
        hScale = height/float(m_height_max);
        samples = width;
    }

    int y_label_inc=15;
    for (int data_set=0;data_set<m_data.length();data_set++) {

        if (m_visibility[data_set]==false) {
            continue;
        }

        if (data_set<m_colors.length())
            pen.setColor(m_colors[data_set]);
        else
            pen.setColor(DEFAULT_COLOR);

        painter.setPen(pen);
        painter.setCompositionMode(QPainter::CompositionMode_Multiply);

        myPolygon.clear();
        myPolygon << QPoint(xRight, yBottom) << QPoint(xLeft, yBottom);
        int prev_y=0;
        for( int elem=0; elem<samples; ++elem ) {
            if (data_set_size < width) {
                myPolygon << QPoint(xLeft+wScale*elem, yTop+hScale*(m_height_max-m_data[data_set][elem]));
            } else {
                double v=0;
                if (m_sampling) {
                    double next_y=(int)wScale*elem;
                    for (int y=prev_y+1;y<=next_y;y++){
                        v+=m_data[data_set][y];
                    }
                    v/=(next_y-prev_y);
                    prev_y = next_y;
                } else {
                    v = m_data[data_set][wScale*elem];
                }
                myPolygon << QPoint(xLeft+elem, yTop+hScale*(m_height_max-v));
            }
        }
        painter.drawPolygon(myPolygon);
        painter.drawText(xLeft+2, yTop+y_label_inc, tr("%1").arg(m_labels[data_set]));
        y_label_inc+=15;
    }
}
