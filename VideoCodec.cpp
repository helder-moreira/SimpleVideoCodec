#include "BitStream.h"
#include "Golomb.h"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <sndfile.h>
#include <time.h>
#include <getopt.h>
#include <fstream>
#include <opencv2/opencv.hpp>
#include <sys/stat.h>

#include <QApplication>
#include <Histogram/mainwindow.h>
#include <QVector>
#include "CAV_Video.h"
#include "BitStream.h"
#include "Golomb.h"

#define ENCODE 101
#define DECODE 102
#define RESIZE 103
#define COMPARE 104


using namespace cv;
using namespace std;
using namespace Cav;

int op = 0;
bool watch = false, device = false, intra_only=false, lossy=false;
int resize_factor = 0,search_area=4,threshold_value=-1,block_size=64;
float lossyY,lossyCb,lossyCr;
Scale resize_op;
DpcmSelection dpcm_op = v4;
char* filein = NULL;
char* fileout = NULL;
clock_t tStart;
double elapsedtime=0;

int printUsage();
void printProgress(int, int);
int parseOpts(int, char**);
long filesize(const char*);
vector<string> &split(const string&, char, vector<string>&);
vector<string> split(const string&, char);

int matMeanWithout00(const Mat&);
void writeMat(GolombEncoder*, const Mat&);
Mat readMat(GolombDecoder*, int, int);

int readDev();
int readFile(char* file);
int compare(char* file1, char* file2, vector<double>*,vector<double>*);
int resize(Scale resize_op, int resize_factor, bool device, bool watch);
int encode();
int decode();

int main (int argc, char *argv[]) {
    if(parseOpts(argc, argv)){
        return -1;
    }

    vector<double> e_vec;
    vector<double> psnr_vec;
    int r;

    tStart  = clock();
    switch(op){
    case COMPARE:
        r = compare(filein, fileout, &e_vec, &psnr_vec);
        if (r==0) {
            QApplication a(argc, argv);
            MainWindow w;
            QVector<int> h_data_e, h_data_psnr;

            if (psnr_vec.size()!=e_vec.size()) {
                cerr << "ops!" << endl;
            }

            h_data_psnr.resize(100);
            h_data_e.resize(100);
            for (int i=0,l=psnr_vec.size();i<l;i++) {
                int ve=round(e_vec[i]);
                int vp;
                if (ve==0) {
                    vp=9999; // like Inf!!
                } else {
                    vp=psnr_vec[i]<0?0:round(psnr_vec[i]);
                }

                while (vp>=h_data_psnr.size() || ve>=h_data_psnr.size()) {
                    h_data_psnr.resize(h_data_psnr.size()+100);
                    h_data_e.resize(h_data_e.size()+100);
                }

                h_data_psnr[vp]+=1;
                h_data_e[ve]+=1;
            }


            HistogramWidget *h = static_cast <HistogramWidget*> (w.centralWidget());
            QList<QString> labels=QList<QString>() <<QString("PSNR")  <<QString("MSE");
            //histogram data
            h->setLabels(labels);
            h->setData(QList<QVector<int> >()   <<h_data_psnr << h_data_e);
            h->setColors(QList<QColor>()        <<Qt::green   << Qt::blue);
            w.show();
            return a.exec();
        }
        return r;
    case ENCODE:
        return encode();
    case DECODE:
        return decode();;
    case RESIZE:
        return resize(resize_op, resize_factor, device, watch);
    default:
        if(device){
            return readDev();
        }else{
            return readFile(filein);
        }
    }
}

int matMeanWithout00(const Mat& mat){
    int mean = 0, temp;
    const int *m;
    for (int i = 0; i < mat.rows; i++){
        m = mat.ptr<const int>(i);
        for(int j = 0; j < mat.cols; j++){
            if (i==0 && j==0) {
                continue;
            }
            temp = m[j];
            if(temp < 0){
                temp = (temp*-2) -1;
            }
            else{
                temp *= 2;
            }
            mean += temp;
        }
    }
    return mean/(mat.cols*mat.rows);
}

int encode(){
    VideoCapture inputVideo;
    RGB8video* cav = NULL;
    int i=0,nframes;

    if (device){
        inputVideo = VideoCapture(0);
        if (!inputVideo.isOpened()) {
            cout << "Could not open camera: " << endl;
            return -1;
        }
        if(block_size > inputVideo.get(CV_CAP_PROP_FRAME_WIDTH) || block_size > inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT)){
            cerr << "ERROR: block size cannot be bigger than width or height of the input video" << endl;
            return -1;
        }
    }else{
        cav = new RGB8video(filein);
        if (!cav->is_open())  {
            return -1;
        }
        if (!cav->read_header())  {
            return -2;
        }
        if(block_size > cav->lines() || block_size > cav->columns()){
            cerr << "ERROR: block size cannot be bigger than width or height of the input video" << endl;
            return -1;
        }
    }

    if (!device){
        nframes = cav->frames();
    }

    Mat image;
    YCbCrFrame YUVframe;
    CodedFrame frame;
    vector<CodedFrame> frames_to_encode;
    int Y_mean = 0, Cb_mean = 0, Cr_mean = 0;

    MotionCompensation mc(search_area, block_size, threshold_value, dpcm_op);
    if(lossy){
        mc.setLossy(lossyY,lossyCb,lossyCr);
    }

    cout << "Reading frames from input and estimate M parameter for Golomb... " << endl;
    while(device || i < nframes){
        if(device){
            inputVideo >> image;
        }else{
            image = cav->nextFrame();
        }

        if (image.empty()) break;

        YUVframe = YCbCrFrame(image);
        if(intra_only){
            if(lossy){
                frame = CodedFrame(YUVframe.dpcmEncodeLossy(dpcm_op,lossyY,lossyCb,lossyCr));
            } else{
                frame = CodedFrame(YUVframe.dpcmEncode(dpcm_op));
            }
        }
        else{
            frame = mc.getCodedFrame(YUVframe);
        }

        frames_to_encode.push_back(frame);
        Y_mean += matMeanWithout00(*frame.Y());
        Cb_mean += matMeanWithout00(*frame.Cb());
        Cr_mean += matMeanWithout00(*frame.Cr());


        if(!device){
            printProgress(i, nframes);
        }
        i++;

        if(watch || device){
            imshow("Display Image", image);
            if (waitKey(5) >= 0 && device ) {
                break;
            }
        }
    }
    if(!device){
        cav->close();
    }else{
        nframes = i;
    }

    Y_mean /= nframes;
    Cb_mean /= nframes;
    Cr_mean /= nframes;

    int m_Y = pow(2,ceil(log2(Y_mean/2)));
    if (m_Y == 0) { m_Y++;}
    int m_Cb = pow(2,ceil(log2(Cb_mean/2)));
    if (m_Cb == 0) { m_Cb++;}
    int m_Cr = pow(2,ceil(log2(Cr_mean/2)));
    if (m_Cr == 0) { m_Cr++;}

    //Create bitstream and golomb encoder for each channel
    BitStream bitstream(fileout,"w");
    GolombEncoder* golombTypes = new GolombEncoder(bitstream, 1, false);
    GolombEncoder* golombVectorRow = new GolombEncoder(bitstream, 1, false);
    GolombEncoder* golombVectorCol = new GolombEncoder(bitstream, 1, false);
    GolombEncoder* golombY = new GolombEncoder(bitstream, m_Y, true);
    GolombEncoder* golombCb = new GolombEncoder(bitstream, m_Cb, true);
    GolombEncoder* golombCr = new GolombEncoder(bitstream, m_Cr, true);

    //Write header
    bitstream.WriteString(to_string(nframes));
    bitstream.WriteString(to_string(image.rows));
    bitstream.WriteString(to_string(image.cols));
    bitstream.WriteString(to_string(m_Y));
    bitstream.WriteString(to_string(m_Cb));
    bitstream.WriteString(to_string(m_Cr));
    bitstream.WriteString(to_string(dpcm_op));
    bitstream.WriteString(to_string(block_size));
    bitstream.WriteString(to_string((int)intra_only));
    bitstream.WriteString(to_string((int)lossy));
    if(lossy){
        bitstream.WriteString(to_string(lossyY));
        bitstream.WriteString(to_string(lossyCb));
        bitstream.WriteString(to_string(lossyCr));
    }
    if(!intra_only){
        int blocks_per_frame = ceil(1.0*image.rows/block_size)*ceil(1.0*image.cols/block_size)
                + 2*(ceil(1.0*(image.rows/2)/ceil(1.0*block_size/2))*ceil(1.0*(image.cols/2)/ceil(1.0*block_size/2)));
        bitstream.WriteString(to_string(blocks_per_frame));
    }

    cout << "Writing to output file..." << endl;
    for(int i = 0; i < nframes; i++){
        CodedFrame frame = frames_to_encode[i];
        writeMat(golombY,*frame.Y());
        writeMat(golombCb,*frame.Cb());
        writeMat(golombCr,*frame.Cr());
        if(!intra_only){
            FrameType type = frame.getType();
            golombTypes->WriteInt((int)type);
            if(type == Pframe){
                vector<int> rows_locations = frame.getRowLocations();
                vector<int> cols_locations = frame.getColLocations();
                golombVectorRow->SetParameter(1);
                golombVectorRow->SetParameter(1);
                for(unsigned int j = 0; j < rows_locations.size(); j++){
                    golombVectorRow->WriteInt(rows_locations[j]);
                    golombVectorCol->WriteInt(cols_locations[j]);
                    if(rows_locations[j] > 1){
                        golombVectorRow->SetParameter(rows_locations[j]);
                    }
                    if(cols_locations[j] > 1){
                        golombVectorCol->SetParameter(cols_locations[j]);
                    }
                }
            }
        }
        printProgress(i,nframes);
    }
    bitstream.Close();

    cout << endl << "Performance results: " << endl;
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "     Elapsed time: " << elapsedtime << " seconds" << endl;
    if(filein != NULL && fileout != NULL){
        double compress_ratio = 1.0 * filesize(filein) / filesize(fileout);
        cout << "     Compress ratio: " << compress_ratio << endl;
    }
    cout << endl;

    delete cav;
    delete golombTypes;
    delete golombVectorRow;
    delete golombVectorCol;
    delete golombY;
    delete golombCb;
    delete golombCr;

    return 0;
}

void writeMat(GolombEncoder* golomb, const Mat& mat){
    const int *m;
    for (int i = 0; i < mat.rows; i++){
        m = mat.ptr<const int>(i);
        for(int j = 0; j < mat.cols; j++){
            golomb->WriteInt(m[j]);
        }
    }
}

int decode(){
    BitStream bitstream(filein,"r");
    RGB8video* out = NULL;
    int m_Y, m_Cb, m_Cr, nframes, rows, cols,blocks_per_frame;

    //read header
    nframes = atoi(bitstream.ReadString().c_str());
    rows = atoi(bitstream.ReadString().c_str());
    cols = atoi(bitstream.ReadString().c_str());
    m_Y = atoi(bitstream.ReadString().c_str());
    m_Cb = atoi(bitstream.ReadString().c_str());
    m_Cr = atoi(bitstream.ReadString().c_str());
    dpcm_op = (DpcmSelection) atoi(bitstream.ReadString().c_str());
    block_size = atoi(bitstream.ReadString().c_str());
    intra_only = (bool)atoi(bitstream.ReadString().c_str());
    lossy = (bool)atoi(bitstream.ReadString().c_str());
    if(lossy){
        lossyY = atof(bitstream.ReadString().c_str());
        lossyCb = atof(bitstream.ReadString().c_str());
        lossyCr = atof(bitstream.ReadString().c_str());
    }
    if(!intra_only){
        blocks_per_frame = atoi(bitstream.ReadString().c_str());
    }


    if(fileout != NULL){
        out = new RGB8video(fileout, true);
        out->write_header(rows, cols);
    }

    GolombDecoder* golombTypes = new GolombDecoder(bitstream, 1, false);
    GolombDecoder* golombVectorRow = new GolombDecoder(bitstream, 1, false);
    GolombDecoder* golombVectorCol = new GolombDecoder(bitstream, 1, false);
    GolombDecoder* golombY = new GolombDecoder(bitstream, m_Y,true);
    GolombDecoder* golombCb = new GolombDecoder(bitstream, m_Cb,true);
    GolombDecoder* golombCr = new GolombDecoder(bitstream, m_Cr,true);

    MotionCompensation mc(block_size, dpcm_op);
    if(lossy){
        mc.setLossy(lossyY,lossyCb,lossyCr);
    }
    Mat image, Y, Cb,Cr;
    CodedFrame frame;
    YCbCrFrame YUVframe;
    cout << "Decoding file..." << endl;
    for(int i = 0; i < nframes; i++){
        Y = readMat(golombY,rows,cols);
        Cb = readMat(golombCb,rows/2,cols/2);
        Cr = readMat(golombCr,rows/2,cols/2);
        frame = CodedFrame(Y,Cb,Cr);
        if(!intra_only){
            FrameType type = (FrameType)golombTypes->ReadInt();
            frame.setType(type);
            if(type == Pframe){
                vector<int> rows_locations;
                vector<int> cols_locations;
                golombVectorRow->SetParameter(1);
                golombVectorRow->SetParameter(1);
                int row,col;
                for(int j = 0; j < blocks_per_frame; j++){
                    row = golombVectorRow->ReadInt();
                    col = golombVectorCol->ReadInt();
                    rows_locations.push_back(row);
                    cols_locations.push_back(col);
                    if(row > 1){
                        golombVectorRow->SetParameter(row);
                    }
                    if(col > 1){
                        golombVectorCol->SetParameter(col);
                    }
                }
                frame.setRowLocations(rows_locations);
                frame.setColLocations(cols_locations);
            }
        }
        if(intra_only){
            if(lossy){
                YUVframe = frame.getYCbCrFrame().dpcmDecodeLossy(dpcm_op,lossyY,lossyCb,lossyCr);
            } else{
                YUVframe = frame.getYCbCrFrame().dpcmDecode(dpcm_op);
            }
        }else{
            YUVframe = mc.getOriginalFrame(frame);
        }

        image = YUVframe.toRGB8();

        if(fileout != NULL){
            out->writeFrame(image);
        }

        printProgress(i,nframes);

        if(watch){
            imshow("Display Image", image);
            waitKey(5);
        }
    }
    bitstream.Close();
    if(fileout != NULL){
        out->close();
    }

    cout << endl << "Performance results: " << endl;
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "     Elapsed time: " << elapsedtime << " seconds" << endl << endl;

    delete golombTypes;
    delete golombVectorRow;
    delete golombVectorCol;
    delete golombY;
    delete golombCb;
    delete golombCr;
    delete out;

    return 0;
}

Mat readMat(GolombDecoder* golomb, int rows, int cols){
    Mat mat(Size(cols,rows),CV_32S);
    int* Mi;
    for(int i=0;i<mat.rows;i++) {
        Mi = mat.ptr<int>(i);
        for(int j=0;j<mat.cols;j++) {
            Mi[j] = golomb->ReadInt();
        }
    }
    return mat;
}

int resize(Scale resize_op, int resize_factor, bool device, bool watch){

    VideoCapture inputVideo;
    RGB8video* cav = NULL;
    RGB8video* out = NULL;

    int rows, columns, new_rows, new_columns,i, nframes;

    if (device){
        inputVideo = VideoCapture(0);
        if (!inputVideo.isOpened()) {
            cout << "Could not open camera: " << endl;
            return -1;
        }
        columns = inputVideo.get(CV_CAP_PROP_FRAME_WIDTH);
        rows = inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT);
    }else{
        cav = new RGB8video(filein);
        if (!cav->is_open())  {
            return -1;
        }
        if (!cav->read_header())  {
            return -2;
        }
        rows = cav->lines();
        columns = cav->columns();
    }

    if (!device){
        i = 0;
        nframes = cav->frames();
    }

    RGB8video::getNewSizes(resize_op, resize_factor,rows,columns, &new_rows,&new_columns);
    if(fileout != NULL){
        out = new RGB8video(fileout, true);
        out->write_header(new_rows, new_columns);
    }

    Mat image;
    while(device || i < nframes){
        if(device){
            inputVideo >> image;
        }else{
            image = cav->nextFrame();
        }

        if (image.empty()) break;

        image = RGB8video::scaleFrame(image,resize_op, resize_factor);

        if(fileout != NULL){
            out->writeFrame(image);
        }

        if(!device){
            printProgress(i++, nframes);
        }

        if(watch || device){
            imshow("Display Image", image);
            if (waitKey(5) >= 0 && device ) {
                break;
            }
        }
    }
    if(!device){
        cav->close();
    }
    if(fileout != NULL){
        out->close();
    }

    cout << endl << "Performance results: " << endl;
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "     Elapsed time: " << elapsedtime << " seconds" << endl;
    if(filein != NULL && fileout != NULL){
        double compress_ratio = 1.0 * filesize(filein) / filesize(fileout);
        cout << "     Compress ratio: " << compress_ratio << endl;
    }
    cout << endl;

    delete out;
    delete cav;
    return 0;
}


void compareFrames(const Mat & frame1, const Mat & frame2, int rows,int columns, double*e, double*psnr) {
    int sqr_a=255*255;
    *e=0;
    *psnr=0;

    if(frame1.size!=frame2.size) {
        return;
    }

    const unsigned char *m1, *m2;
    for(int i=0;i<rows;i++) {
        m1 = frame1.ptr<const unsigned char>(i);
        m2 = frame2.ptr<const unsigned char>(i);
        for(int j=0;j<columns*3;j++) {
            *e+=((m1[j]-m2[j])*(m1[j]-m2[j]));
        }
    }

    *e /= (rows * columns * 3);
    *psnr=10*log10(sqr_a / *e);
}

int compare(char* file1, char* file2,vector<double>* e_vec,vector<double>* psnr_vec){
    RGB8video cav1(file1);
    RGB8video cav2(file2);

    if (!cav1.is_open() || !cav2.is_open())  {
        return -1;
    }

    if (!cav1.read_header() || !cav2.read_header())  {
        return -2;
    }

    if (!cav1.frames() != !cav2.frames()){
        return -3;
    }

    Mat image1;
    Mat image2;
    double mean_e=0, mean_psnr=0;
    int i = 0, nframes = cav1.frames();
    while (cav1.hasNextFrame()) {
        image1 = cav1.nextFrame();
        image2 = cav2.nextFrame();

        if (image1.empty()) break;
        if (image2.empty()) break;

        double e,psnr;
        compareFrames(image1,image2,cav1.lines(),cav2.columns(),&e,&psnr);
        e_vec->push_back(e);
        psnr_vec->push_back(psnr);
        mean_e += e;
        mean_psnr += psnr;
        printProgress(i++, nframes);
    }
    cav1.close();
    cav2.close();

    mean_e /= cav1.frames();
    mean_psnr /= cav1.frames();
    cout << endl << "Mean of MSEs: " << mean_e << "  Mean of PSNRs: " << mean_psnr << endl;
    cout << endl << "Performance results: " << endl;
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "     Elapsed time: " << elapsedtime << " seconds" << endl;
    cout << endl;
    return 0;
}

int readDev() {
    Mat image;
    VideoCapture inputVideo(0);              // Open input
    RGB8video* out = NULL;

    if (!inputVideo.isOpened()) {
        cout << "Could not open camera: " << endl;
        return -1;
    }

    int columns = inputVideo.get(CV_CAP_PROP_FRAME_WIDTH);
    int rows = inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT);

    if(fileout != NULL){
            out = new RGB8video(fileout, true);
            out->write_header(rows, columns);
        }

    for (;; ) { //Show the image captured in the window and repeat
        inputVideo >> image;                    // read
        if (image.empty()) break;               // check if at end

        imshow("Display Image", image);

        if(fileout != NULL){
            out->writeFrame(image);
        }

        if (waitKey(5) >= 0) break;
    }
    if(fileout != NULL){
        out->close();
    }
    return 0;
}

int readFile(char* file) {
    RGB8video cav(file);

    if (!cav.is_open())  {
        return -1;
    }

    if (!cav.read_header())  {
        return -2;
    }

    Mat image;

    int i = 0, nframes = cav.frames();
    while (cav.hasNextFrame()) {
        image = cav.nextFrame();

        if (image.empty()) break;

        imshow("Display Image", image);

        if (waitKey(5) >= 0) break;
        printProgress(i++, nframes);
    }
    cav.close();

    cout << endl << "Performance results: " << endl;
    elapsedtime= (double)(clock() - tStart)/CLOCKS_PER_SEC;
    cout << "     Elapsed time: " << elapsedtime << " seconds" << endl;
    cout << endl;

    return 0;
}

void printProgress(int n, int total){
    if( total < 100 || n % (total/100) == 0 ){
        int percent =  ceil(n*100.0/total) +1;
        fprintf(stderr, "\r   Progress: [%s%s] %2d%%",
                string( percent/2 ,'#').c_str(),
                string( 50-(percent/2) ,'-').c_str(),
                percent);
        if (percent == 100){
            fprintf(stderr, "\n");
        }
    }
}

int printUsage(){
    fprintf(stderr, "Usage:\n\t./VideoCodec "
                    "[-g] [-i <input file>] [-o <output file>] [-r op[=factor]] [-e [OPTIONAL PARAMETERS] /-d] [-w] [-h]\n\n"
                    "\t-c\t: Compare operation: compares input file with the output file\n"
                    "\t-e\t: Encode operation. Parameters:\n"
                    "\t\t\t\t-f\t: Enable intra-frame coding only\n"
                    "\t\t\t\t-v\t: Set JPEG version (0 to 7) (default is v4)\n"
                    "\t\t\t\t-t\t: Set threshold for inter-frame coding (default is dynamically set)\n"
                    "\t\t\t\t-s\t: Set search area (default is 4)\n"
                    "\t\t\t\t-b\t: Set block size (default is 64)\n"
                    "\t\t\t\t-l\t: Enable lossy with factors as Y:Cb:Cr or one value for all\n"
                    "\t-d\t: Decode operation\n"
                    "\t-r\t: Resize operation. Parameters: (operation[=factor] (default factor is 2))\n"
                    "\t\t\t\texp\t: Expand operation\n"
                    "\t\t\t\trtl\t: Reduce to top left pixel\n"
                    "\t\t\t\trtm\t: Reduce to mean of pixels\n"
                    "\t-i\t: Set input file\n"
                    "\t-g\t: Set digital camera as input\n"
                    "\t-o\t: Set output file\n"
                    "\t-w\t: Watch the video\n"
                    "\t-h\t: Shows this menu\n\n");
    return -1;
}

int parseOpts(int argc, char** argv) {
    int c,temp_i;
    char* temp;
    vector<string> lossy_factors;
    cout << endl;
    while ((c = getopt(argc, argv, "cgedwi:o:r:v:ft:s:b:l:")) != -1) {
        switch (c) {
        case 'd':
            op = DECODE;
            break;
        case 'e':
            op = ENCODE;
            break;
        case 'f':
            intra_only = true;
            break;
        case 't':
            if(atoi(optarg) < 0){
                cerr << "WARNING: Threshold_value must be positive. Setting to default value." << endl;
            }else{
                threshold_value = atoi(optarg);
            }
            break;
        case 'l':
            lossy_factors = split(optarg,':');
            if(lossy_factors.size() !=3 && lossy_factors.size() != 1){
                cerr << "ERROR: Lossy factors must be set for Y Cb and Cr as 'Y:Cb:Cr' or one value for all" << endl << endl;
                return printUsage();
            }else{
                lossy = true;
                if(lossy_factors.size() == 1){
                    lossyY = atof(lossy_factors[0].c_str());
                    lossyCb = atof(lossy_factors[0].c_str());
                    lossyCr = atof(lossy_factors[0].c_str());
                }else{
                    lossyY = atof(lossy_factors[0].c_str());
                    lossyCb = atof(lossy_factors[1].c_str());
                    lossyCr = atof(lossy_factors[2].c_str());
                }
                if(lossyY < 1 || lossyCb < 1 || lossyCr < 1){
                    cerr << "ERROR: Lossy factors must be greater or equal than 1." << endl << endl;
                    return printUsage();
                }
            }
            break;
        case 'b':
            if(atoi(optarg) <=0){
                cerr << "WARNING: Block size must be positive. Setting to default value." << endl;
            }else{
                block_size = atoi(optarg);
            }
            break;
        case 's':
            if(atoi(optarg) <0){
                cerr << "WARNING: Search area value must be positive or zero. Setting to default value." << endl;
            }else{
                search_area = atoi(optarg);
            }
            break;
        case 'v':
            temp_i = atoi(optarg);
            if ( temp_i <= 7 && temp_i >= 0 ) {
                dpcm_op = (DpcmSelection)temp_i;
            }
            else{
                return printUsage();
            }
            break;
        case 'r':
            op = RESIZE;
            temp = optarg;
            break;
        case 'c':
            op = COMPARE;
            break;
        case 'i':
            filein = optarg;
            break;
        case 'g':
            device = true;
            break;
        case 'o':
            fileout = optarg;
            break;
        case 'w':
            watch=true;
            break;
        case 'h':
        default:
            return printUsage();
        }
    }
    cout << endl;
    if(op == 0 && !watch){
        return printUsage();
    }
    switch(op){
    case COMPARE:
        if (filein == NULL || fileout == NULL){
            return printUsage();
        }
        break;
    case ENCODE:
        if ((filein == NULL && !device) || fileout == NULL){
            return printUsage();
        }
        break;
    case DECODE:
        if (filein == NULL || (fileout == NULL && !watch)){
            return printUsage();
        }
        break;
    case RESIZE:
    {
        if ((filein == NULL && !device) || (fileout == NULL && !watch)){
            return printUsage();
        }
        string s(temp);
        if(s.substr(0, 3).compare("exp") == 0){
            resize_op = Expansion;
        }
        else {
            if (s.substr(0, 3).compare("rtl") == 0){
                resize_op = ReductionToTopLeft;
            }
            else{
                if (s.substr(0, 3).compare("rtm") == 0){
                    resize_op = ReductionToMean;
                }else{
                    return printUsage();
                }
            }
        }
        if(s.length() > 4){
            resize_factor = atoi(s.substr(4, s.length()).c_str());
        }
        if (resize_factor == 0){
            resize_factor = 2;
        }
        break;
    }
    default:
        if(!watch || (filein == NULL && !device)){
            return printUsage();
        }
        break;
    }
    return 0;
}

vector<string> &split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

long filesize(const char* filename)
{
    struct stat stat_buf;
    int rc = stat(filename, &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}
